import java.util.*;

public class PhotoParser {
    public static List<Photo> ParsePhotos(List<String> lines) {
        ArrayList<Photo> photos = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            String[] splitLine = lines.get(i).split(" ");
            int ammountTags = Integer.parseInt(splitLine[1]);
            Set<String> tags = new HashSet<>();

            tags.addAll(Arrays.asList(splitLine).subList(2, ammountTags + 2));

            photos.add(new Photo(i, Orientation.valueOf(splitLine[0]), tags));
        }
        return photos;
    }
}
