import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
//        generateSlideshow("a_example.txt", "a_example.out.txt");

//        generateSlideshow("b_lovely_landscapes.txt", "b_lovely_landscapes.out.txt");

//        generateSlideshow("c_memorable_moments.txt", "c_memorable_moments.out.txt");

        generateSlideshow("d_pet_pictures.txt", "d_pet_pictures.out.txt");

//        generateSlideshow("e_shiny_selfies.txt", "e_shiny_selfies.out.txt");
    }

    private static void generateSlideshow(String in, String out) throws IOException {
        System.out.println("============== Generating Slideshow for "+in+" ==============");
        List<Photo> photos = readPhotos(in);

        SlideshowGenerator slideshowGenerator = new MyFirstSlideshowGenerator(photos);

        List<Slide> slides = slideshowGenerator.generateSlideshow();

        writeSlideshow(out, slides);
        System.out.println("============== Finished Slideshow for "+in+" ==============");

    }

    private static List<Photo> readPhotos(String in) throws IOException {
        File file = new File(Main.class.getClassLoader().getResource(in).getFile());
        List<String> lines = FileUtils.readLines(file, "ASCII");
        lines.remove(lines.get(0));
        return PhotoParser.ParsePhotos(lines);
    }

    private static void writeSlideshow(String filename, List<Slide> slides) throws IOException {
        ArrayList<String> out = new ArrayList<>();
        out.add(""+slides.size());
        for(Slide slide : slides) {
            StringBuilder outString = new StringBuilder();
            for(Photo photo : slide.getPhotos()) {
                outString.append(photo.getId()).append(" ");
            }
            outString = new StringBuilder(outString.toString().trim());
            out.add(outString.toString());
        }
        FileUtils.writeLines(new File(filename), out);
    }
}
