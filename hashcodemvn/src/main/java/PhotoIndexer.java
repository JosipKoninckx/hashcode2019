import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;

public class PhotoIndexer {

    public Multimap<String, Photo> index(Collection<Photo> photos) {
        HashMultimap<String, Photo> result = HashMultimap.create();

        for (Photo photo : photos) {
            for (String tag : photo.getTags()) {
                result.put(tag, photo);
            }
        }
        return result;
    }

}
