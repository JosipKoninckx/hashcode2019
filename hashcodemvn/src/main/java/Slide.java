import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@ToString
public class Slide {
    private List<Photo> photos;
    private Set<String> tags;

    public Slide(List<Photo> photos) {
        this.photos = photos;
        this.tags = generateTags();
    }

    private Set<String> generateTags() {
        if (photos.size() == 1) {
            return photos.get(0).getTags();
        }
        Set<String> tags = new HashSet<>();
        tags.addAll(photos.get(0).getTags());
        tags.addAll(photos.get(1).getTags());
        return new HashSet<>(tags);
    }
}
