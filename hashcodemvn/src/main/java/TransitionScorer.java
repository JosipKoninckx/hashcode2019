import com.google.common.collect.Sets;

import java.util.Set;

public class TransitionScorer {

    int compare(Set<String> srcTags, Set<String> targetTags) {
        int intersection = Sets.intersection(srcTags, targetTags).size();
        int diffSrc = srcTags.size() - intersection;
        int diffTarget = targetTags.size() - intersection;

        return Math.min(intersection, Math.min(diffSrc, diffTarget));
    }

}
