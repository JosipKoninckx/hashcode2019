import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MyFirstSlideshowGenerator implements SlideshowGenerator {

    Set<Photo> availablePhotos;
    Multimap<String, Photo> index;
    TransitionScorer comparator = new TransitionScorer();

    public MyFirstSlideshowGenerator(List<Photo> initialPhotos) {
        this.availablePhotos = Sets.newHashSet(initialPhotos);
        index = new PhotoIndexer().index(initialPhotos);

    }

    @Override
    public List<Slide> generateSlideshow() {
        List<Slide> result = new ArrayList<>();
        Optional<Slide> currentSlide = getSlide(availablePhotos);
        while (currentSlide.isPresent()) {
            result.add(currentSlide.get());
            Optional<Slide> nextSlide = getNextSlide(currentSlide.get());
            if(nextSlide.isPresent()) {
                currentSlide = nextSlide;
            }
            else {
                currentSlide = getAvailableSlide();
            }
            if((result.size() % 100) == 0 ) {
                System.out.print("Generated slides = "+result.size()+"\r");
            }
        }
        System.out.println();
        return result;
    }

    private Optional<Slide> getAvailableSlide() {
        return getSlide(availablePhotos);
    }

    private Optional<Slide> getSlide(Collection<Photo> photos) {
        Photo vertPhoto = null;
        for (Photo photo : photos) {
            if(photo.getOrientation().equals(Orientation.H)) {
                this.availablePhotos.remove(photo);
                return Optional.of(new Slide(Collections.singletonList(photo)));
            }
            else {
                if(vertPhoto != null && photo.getOrientation().equals(Orientation.V)) {
                    this.availablePhotos.remove(vertPhoto);
                    this.availablePhotos.remove(photo);
                    return Optional.of(new Slide(Lists.newArrayList(vertPhoto, photo)));
                }
                else {
                    vertPhoto = photo;
                }
            }
        }
        return Optional.empty();
    }

    private Optional<Slide> getNextSlide(Slide slide) {
        Set<String> tags = slide.getTags();
        int scoreThreshold = slide.getTags().size() > 10 ? 2 : 1;
        Iterator<Photo> iterator = tags.stream()
                .flatMap(tag -> index.get(tag).stream())
                .distinct()
                .filter(photo -> availablePhotos.contains(photo))
                .iterator();
        Map<Photo, Integer> scoreMap = new HashMap<>();
        while(iterator.hasNext()) {
            Photo photo = iterator.next();
            int score = comparator.compare(tags, photo.getTags());
            if(score >= scoreThreshold) {
                scoreMap.put(photo, score);
            }
            if(scoreMap.size() >= 50)
                break;
        }

        List<Photo> sortedPhotos = scoreMap.keySet().stream()
                .sorted(Comparator.comparing((Function<Photo, Integer>) scoreMap::get).reversed())
                .collect(Collectors.toList());
        return getSlide(sortedPhotos);
    }
}
